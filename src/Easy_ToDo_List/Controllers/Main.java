package Easy_ToDo_List.Controllers;

import Easy_ToDo_List.Exceptions.DuplicateItemException;
import Easy_ToDo_List.Exceptions.EmptyDescriptionException;
import Easy_ToDo_List.Model.ListItem;
import Easy_ToDo_List.Model.ToDoList;
import Easy_ToDo_List.Views.MainViewController;
import Easy_ToDo_List.Views.MainViewListener;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Optional;

public class Main extends Application implements MainViewListener
{
    private ToDoList toDoList;
    private MainViewController controller;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        toDoList = new ToDoList();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/MainView.fxml"));
        loader.load();
        Parent root = loader.getRoot();

        controller = loader.getController();
        controller.setToDoList(toDoList);
        controller.setListener(this);

        primaryStage.setTitle("Easy To-Do List");
        primaryStage.setScene(new Scene(root, 450, 400));
        primaryStage.show();
    }


    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void onRemoveItemClick(ListItem item)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Voulez-vous vraiment supprimer ?");
        Optional<ButtonType> buttonType = alert.showAndWait();

        if(buttonType.get() == ButtonType.OK)
        {
            toDoList.removeItem(item);
            controller.updateList();
        }
    }

    @Override
    public void onAddItemClick(String description)
    {
        try
        {
            this.toDoList.addItem(description);
            this.controller.clearTextField();
            this.controller.updateList();
        }
        catch(EmptyDescriptionException ex)
        {
            Alert a = new Alert(Alert.AlertType.ERROR, ex.getMessage());
            a.showAndWait();
        }
        catch(DuplicateItemException ex)
        {
            Alert a = new Alert(Alert.AlertType.ERROR, ex.getMessage());
            a.showAndWait();
        }
    }

    @Override
    public void onCheckBoxClicked(ListItem item)
    {
        item.toggleDone();
    }
}
