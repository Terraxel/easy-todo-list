package Easy_ToDo_List.Model;

import Easy_ToDo_List.Exceptions.EmptyDescriptionException;

public class ListItem
{
    protected String description;
    protected boolean done;

    public ListItem(String description)
    {
        if(description.trim().isEmpty())
            throw new EmptyDescriptionException();
        else
        {
            this.description = description;
            this.done = false;
        }

    }

    public boolean isDone()
    {
        return this.done;
    }

    public void toggleDone()
    {
        this.done = !this.done;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getLowerCaseDescription()
    {
        return description.toLowerCase();
    }
}
