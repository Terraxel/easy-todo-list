package Easy_ToDo_List.Model;

import Easy_ToDo_List.Exceptions.DuplicateItemException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ToDoList
{
    protected ArrayList<ListItem> items;

    public ToDoList()
    {
        this.items = new ArrayList<>();
    }

    public void addItem(String description)
    {
        if(checkDuplicateItem(description))
            throw new DuplicateItemException();
        else
            this.items.add(new ListItem(description.trim()));
    }

    public void removeItem(ListItem item)
    {
        items.remove(item);
    }

    public List<ListItem> getItems()
    {
        return Collections.unmodifiableList(items);
    }

    private boolean checkDuplicateItem(String description)
    {
        for(ListItem i : this.items)
        {
            if(i.getDescription().equalsIgnoreCase(description.trim()))
                return true;
        }

        return false;
    }
}
