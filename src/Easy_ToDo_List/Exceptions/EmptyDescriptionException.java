package Easy_ToDo_List.Exceptions;

public class EmptyDescriptionException extends RuntimeException
{
    public EmptyDescriptionException()
    {
        super("Description vide !");
    }
}
