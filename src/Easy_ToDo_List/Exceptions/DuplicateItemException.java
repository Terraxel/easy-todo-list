package Easy_ToDo_List.Exceptions;

public class DuplicateItemException extends RuntimeException
{
    public DuplicateItemException()
    {
        super("Un élément du même nom existe déjà !");
    }
}
