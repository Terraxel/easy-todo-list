package Easy_ToDo_List.Views;

import Easy_ToDo_List.Model.ListItem;
import Easy_ToDo_List.Model.ToDoList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.util.List;

public class MainViewController
{
    @FXML
    VBox itemList;
    @FXML
    TextField itemTextField;

    ToDoList toDoList;
    MainViewListener listener;

    public void setToDoList(ToDoList toDoList)
    {
        this.toDoList = toDoList;
        this.updateList();
    }

    public void setListener(MainViewListener mvListener)
    {
        this.listener = mvListener;
    }

    public void updateList()
    {
        itemList.getChildren().clear();

        for(ListItem item : toDoList.getItems())
        {
            itemList.getChildren().add(getItemView(item));
        }
    }

    public void onAddItemClick()
    {
        listener.onAddItemClick(this.itemTextField.getText());
    }

    public void onKeyPressed(KeyEvent e)
    {
        if(e.getCode() == KeyCode.ENTER)
            onAddItemClick();
    }

    public void onCheckBoxClicked(ListItem item)
    {
        listener.onCheckBoxClicked(item);
    }

    public void clearTextField()
    {
        this.itemTextField.clear();
    }

    private BorderPane getItemView(ListItem item)
    {
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(5));
        borderPane.setLeft(getCheckBoxItem(item));
        borderPane.setCenter(new Label(item.getDescription()));

        borderPane.setRight(getDeleteButtonItem(item));

        return borderPane;
    }

    private CheckBox getCheckBoxItem(ListItem item)
    {
        CheckBox checkBox = new CheckBox();
        checkBox.setSelected(item.isDone());
        BorderPane.setMargin(checkBox, new Insets(5, 0, 0, 5));

        checkBox.setOnMouseClicked(e -> onCheckBoxClicked(item));

        checkBox.setStyle("");

        return checkBox;
    }

    private Button getDeleteButtonItem(ListItem item)
    {
        Button b = new Button("X");
        b.setOnMouseClicked(e -> listener.onRemoveItemClick(item));

        b.setStyle("-fx-background-color: indianred");

        return b;
    }
}
