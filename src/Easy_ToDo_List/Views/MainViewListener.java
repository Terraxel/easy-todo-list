package Easy_ToDo_List.Views;

import Easy_ToDo_List.Model.ListItem;

public interface MainViewListener
{
    void onRemoveItemClick(ListItem item);
    void onAddItemClick(String description);
    void onCheckBoxClicked(ListItem item);
}
